watch:
	cargo lambda watch

invoke:
	cargo lambda invoke --data-ascii "{ \"total\": 20}"

build: 
	cargo lambda build --release --arm64

deploy:
	cargo lambda deploy --region us-east-1

### Invoke on AWS
invoke:
	cargo lambda invoke --remote project2 --data-ascii "{ \"total\": 20}"