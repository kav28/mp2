# mp2



# Rust Lambda Function: Add 200 to a Number

This is a simple AWS Lambda function written in Rust. It takes a number as input and returns that number plus 200. The function is designed to be deployed on the AWS Lambda platform and triggered by various events.

## Function Overview

The function consists of the following components:

- **Request Struct**: Deserializes incoming JSON requests containing a `total` field of type `i32`.
- **Response Struct**: Serializes the calculation result, which is the sum of the input number and 200.
- **Function Handler**: The main entry point of the function, where the calculation is performed. It takes an `LambdaEvent` containing the request and returns a `Result<Response, Error>`.

## How to Use

1. **Clone the Repository**: Clone or download this repository to your local machine.
2. **Setup AWS Lambda Environment**: Ensure you have the AWS Lambda Rust Runtime setup on your development environment. You can follow the instructions [here](https://github.com/awslabs/aws-lambda-rust-runtime).
3. **Build the Function**: Build the Rust project using Cargo: cargo build --release
4. **Deploy to AWS Lambda**: Deploy the function to AWS Lambda using the AWS CLI or other deployment methods provided by AWS.
5. **Invoke the Function**: Once deployed, you can invoke the function using various triggers such as API Gateway events, S3 events, or CloudWatch events.

## Additional Resources

- [AWS Lambda Rust Runtime Examples](https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples): Explore additional examples provided by the AWS Lambda Rust Runtime project.
- [Serverless Rust Demo](https://github.com/aws-samples/serverless-rust-demo/): Learn more about building serverless applications with Rust using AWS services.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

